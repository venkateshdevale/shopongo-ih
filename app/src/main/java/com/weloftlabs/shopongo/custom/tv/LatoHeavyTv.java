package com.weloftlabs.shopongo.custom.tv;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Venkatesh Devale
 */
public class LatoHeavyTv extends TextView {
    public LatoHeavyTv(Context context) {
        super(context);
    }

    public LatoHeavyTv(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatoHeavyTv(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Heavy.ttf");
        setTypeface(tf);
    }
}
