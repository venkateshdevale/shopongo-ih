package com.weloftlabs.shopongo.custom.tv;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Venkatesh Devale
 */
public class RobotoMediumTv extends TextView {
    public RobotoMediumTv(Context context) {
        super(context);
    }

    public RobotoMediumTv(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoMediumTv(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Medium.ttf");
        setTypeface(tf);
    }
}
