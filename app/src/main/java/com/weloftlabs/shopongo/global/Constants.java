package com.weloftlabs.shopongo.global;

public class Constants {
    public static final String SHARED_PREFERENCE_KEY = "MY_PREFS";

    public final static String TRNQL_API_KEY = "255a75d5-5214-4294-b430-dbdf11da1265";
    public static final String FB_USER_ID = "fbUserId";
    public static final String USER_NAME = "userName";
    public static final String SEARCH_RANGE = "range";

    public static final String LAT = "lat";
    public static final String LONG = "long";

    public static final String PARSE_APPLICATION_ID = "GL4Kof8Fk5P9BhZNR3b6pva1gUMaiSg2G9Y7nBHJ";
    public static final String PARSE_CLIENT_KEY = "9vvLfvlLFxqLOTRlcGzXPCSSGWX8oYEZCQWHEpy3";

    public static final String HAS_SHOP = "hasShop";
    public static final String SHOP_SETUP = "shotSetup";

    public interface FbUser {
        String FB_USER_ID = "fbUserId";
        String NAME = "name";
        String EMAIL = "email";
        String USER_CONTACT = "userContact";
        String IS_SUBSCRIBED = "isSubscribed";
    }

    public interface ShopDetails {
        String FB_USER_ID = "fbUserId";
        String SHOP_NAME = "ShopName";
        String SHOP_PHONE = "ShopPhone";
        String OWNER_NAME = "OwnerName";
        String SERVICES_PROVIDED = "ServicesProvided";
        String SHOP_TIME_START = "ShopTimeStart";
        String SHOP_CATEGORY = "ShopCategory";
        String MINIMUM_CHARGES = "MinimumCharges";
    }

    public interface ShopRequest {
        String SHOP_ID = "ShopId";
        String CLIENT_FB_ID = "ClientFbId";
        String EXPECTED_TIME = "ExpectedTime";
        String IS_DONE = "isDone";
        String JOB_REQUEST = "JobRequest";
        String CLIENT_ADDRESS = "ClientAddress";
    }
}
