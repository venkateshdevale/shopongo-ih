package com.weloftlabs.shopongo.global;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.trnql.zen.core.AppData;
import com.weloftlabs.shopongo.R;

public class MyApp extends AppData {

    public static String[] sCategoriesList = {"Entertainment", "Food and Beverages",
            "Hardware Works", "Technology", "Tutors", "Personal Care"};

    public static int[] sCategoryImages = {R.drawable.entertainment, R.drawable.food_van1, R.drawable.house_work1
            , R.drawable.tech1, R.drawable.tutor1, R.drawable.personal_care1};

    @Override
    public void onCreate() {
        super.onCreate();

        initSDKs();
    }

    private void initSDKs() {


        FacebookSdk.sdkInitialize(getApplicationContext());

        Parse.initialize(this, Constants.PARSE_APPLICATION_ID, Constants.PARSE_CLIENT_KEY);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
    }


}
