package com.weloftlabs.shopongo.managers;

import android.app.Activity;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.trnql.smart.people.PersonEntry;

import java.util.ArrayList;
import java.util.List;

public class JobsManager {
    private static JobsManager sJobsManager;
    private static Activity sActivity;

    private List<PersonEntry> mPeopleAroundList;
    private List<ParseObject> mShopsAroundList;

    private boolean loadPeopleData = true;

    private JobsManager() {
        sJobsManager = this;
        mPeopleAroundList = new ArrayList<>();
        mShopsAroundList = new ArrayList<>();
    }

    public static JobsManager newInstance(Activity activity) {
        if (sJobsManager == null) {
            new JobsManager();
        }
        sActivity = activity;

        return sJobsManager;
    }

    public void setPeopleAround(List<PersonEntry> peopleAroundList) {
        mPeopleAroundList = peopleAroundList;

        if (mPeopleAroundList.size() != 0)
            loadPeopleData = false;
    }

    public List<PersonEntry> getPeopleAroundList() {
        return mPeopleAroundList;
    }

    public boolean shouldLoadPeople() {
        return loadPeopleData;
    }

    public void setLoadPeopleBool(){
        loadPeopleData = true;
    }

    public void searchUsersInParse() {
        mShopsAroundList = new ArrayList<>();

        for (PersonEntry p : mPeopleAroundList) {
            if (!p.getDataPayload().equalsIgnoreCase("hasShop"))
                continue;

            ParseQuery q = new ParseQuery("ShopDetails");
            q.whereEqualTo("fbUserId", p.getUserToken());
            q.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    mShopsAroundList.add(parseObject);
                }
            });

        }
    }

}
