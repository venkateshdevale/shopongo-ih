package com.weloftlabs.shopongo.managers;

import android.app.Activity;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.weloftlabs.shopongo.global.Constants;

import java.util.ArrayList;
import java.util.List;

public class MyShopManager {

    private static MyShopManager sMyShopManager;
    private static Activity sActivity;
    private List<ParseObject> mOrdersList;
    private List<ParseObject> mClientDetailList;

    private MyShopManager() {
        sMyShopManager = this;
    }

    public static MyShopManager newInstance(Activity activity) {
        if (sMyShopManager == null) {
            new MyShopManager();
        }
        sActivity = activity;

        return sMyShopManager;
    }

    public void getMyShopOrders(final OrdersCallback callback) {
        ParseQuery q = new ParseQuery("ShopRequest");
        q.whereEqualTo(Constants.ShopRequest.SHOP_ID,
                SharedPreferenceManager.newInstance(sActivity).getSavedValue(Constants.FB_USER_ID));
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    mOrdersList = new ArrayList<ParseObject>();
                    mClientDetailList = new ArrayList<ParseObject>();
                    if (list.size() == 0) {
                        callback.success(mOrdersList, mClientDetailList);
                    }
                    for (final ParseObject obj : list) {
                        ParseQuery q = new ParseQuery("FbUser");
                        q.whereEqualTo(Constants.FB_USER_ID, obj.getString(Constants.ShopRequest.CLIENT_FB_ID));
                        q.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject parseObject, ParseException e) {
                                if (e == null) {
                                    mOrdersList.add(obj);
                                    mClientDetailList.add(parseObject);
                                    callback.success(mOrdersList, mClientDetailList);
                                } else {
                                    callback.fail();
                                }
                            }

                        });
                    }

                } else {
                    callback.fail();
                }
            }

        });
    }

    public interface OrdersCallback {
        void success(List<ParseObject> ordersList, List<ParseObject> clientDetailList);

        void fail();
    }

}
