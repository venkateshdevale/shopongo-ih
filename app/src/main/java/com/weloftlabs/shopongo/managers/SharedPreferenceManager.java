package com.weloftlabs.shopongo.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.weloftlabs.shopongo.global.Constants;


public class SharedPreferenceManager {
    private static SharedPreferenceManager sSharedPreferenceManager;
    private SharedPreferences mSharedPreference;

    private SharedPreferenceManager(Context context) {
        mSharedPreference = context.getSharedPreferences(Constants.SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
    }

    public static SharedPreferenceManager newInstance(Context context) {
        if (sSharedPreferenceManager == null) {
            sSharedPreferenceManager = new SharedPreferenceManager(context);
        }

        return sSharedPreferenceManager;
    }

    /**
     * Use to save String value
     *
     * @param key
     * @param value
     */
    public void saveValue(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getSavedValue(String key) {
        return mSharedPreference.getString(key, "");
    }

    /**
     * Use to save boolean state values
     *
     * @param key
     * @param value
     */
    public void saveStateValues(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getSavedStates(String key) {
        return mSharedPreference.getBoolean(key, false);
    }

    /**
     * Use to save int state values
     *
     * @param key
     * @param value
     */
    public void saveStateIntValues(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getSavedIntStates(String key) {
        return mSharedPreference.getInt(key, 0);
    }

}
