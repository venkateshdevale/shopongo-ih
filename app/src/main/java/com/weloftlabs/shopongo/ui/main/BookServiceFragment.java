package com.weloftlabs.shopongo.ui.main;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;

import java.util.Calendar;

public class BookServiceFragment extends DialogFragment implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    TextView mRequestedTime;
    EditText mRequestDescription;
    EditText mMyAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_book_service, container, false);

        getDialog().setTitle("Request Form");

        mRequestedTime = (TextView) root.findViewById(R.id.request_time);
        mRequestDescription = (EditText) root.findViewById(R.id.request_description);
        mMyAddress = (EditText) root.findViewById(R.id.my_address);

        TextView bookService = (TextView) root.findViewById(R.id.book_service);

        mRequestedTime.setOnClickListener(this);
        bookService.setOnClickListener(this);

        mRequestedTime.setText(Html.fromHtml("<b>10:30</b>"));
        mMyAddress.setText(Cookie.sMyAddress);


        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.request_time:
                Calendar now = Calendar.getInstance();
                TimePickerDialog timePickerDialog
                        = new TimePickerDialog(getContext(), this, now.get(Calendar.HOUR_OF_DAY)
                        , now.get(Calendar.MINUTE), true);
                timePickerDialog.show();
                break;
            case R.id.book_service:
                final Dialog d = AlertUtils.showDialog(getContext(), "Placing request.");
                ParseObject obj = new ParseObject("ShopRequest");
                obj.put(Constants.ShopRequest.SHOP_ID, Cookie.sSearchResult.getString(Constants.ShopDetails.FB_USER_ID));
                obj.put(Constants.ShopRequest.JOB_REQUEST, mRequestDescription.getText().toString());
                obj.put(Constants.ShopRequest.EXPECTED_TIME, mRequestedTime.getText().toString());
                obj.put(Constants.ShopRequest.CLIENT_ADDRESS, mMyAddress.getText().toString());
                obj.put(Constants.ShopRequest.CLIENT_FB_ID
                        , SharedPreferenceManager.newInstance(getContext()).getSavedValue(Constants.FB_USER_ID));
                obj.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(getContext(), "Error while booking service, please try again", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext(), "Requested submitted", Toast.LENGTH_LONG).show();
                        }
                        d.dismiss();
                        getDialog().dismiss();
                    }
                });
                break;
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mRequestedTime.setText(Html.fromHtml("<b>" + hourOfDay + ":" + minute + "</b>"));
    }
}
