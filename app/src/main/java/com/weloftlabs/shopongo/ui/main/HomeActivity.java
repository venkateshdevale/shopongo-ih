package com.weloftlabs.shopongo.ui.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.trnql.smart.base.SmartCompatActivity;
import com.trnql.smart.location.AddressEntry;
import com.trnql.smart.people.PeopleManager;
import com.trnql.smart.people.PersonEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.managers.JobsManager;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.ui.myrequests.MyRequestActivity;
import com.weloftlabs.shopongo.ui.shop.ShopActivity;
import com.weloftlabs.shopongo.ui.siren.IceCreamSiren;
import com.weloftlabs.shopongo.utils.FragmentHelper;

import java.util.List;


public class HomeActivity extends SmartCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnClickListener {

    private EditText mLocationEt;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getAppData().setApiKey(Constants.TRNQL_API_KEY);

        initViews();
    }

    private void initViews() {
        Toast.makeText(this, "Keep Internet and GPS turned on to use this app effectively", Toast.LENGTH_LONG).show();
        PeopleManager.INSTANCE.setUserToken(SharedPreferenceManager
                .newInstance(this).getSavedValue(Constants.FB_USER_ID));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mLocationEt = (EditText) toolbar.findViewById(R.id.location_et);
        setSupportActionBar(toolbar);

        /**
         * Setup Navigation Drawers here
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        try {
            View header = navigationView.getHeaderView(0);
            ((TextView) header.findViewById(R.id.name))
                    .setText(SharedPreferenceManager.newInstance(this).getSavedValue(Constants.USER_NAME));
        } catch (Exception e) {

        }
        FragmentHelper.replaceAndAddToStack(this, R.id.container, HomeFragment.newInstance());

        refresh();
    }

    public void refresh() {
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                JobsManager.newInstance(HomeActivity.this).setLoadPeopleBool();

                refresh();
            }
        }, 120000);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof HomeFragment) {
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_requests) {
            startActivity(new Intent(this, MyRequestActivity.class));
        } else if (id == R.id.my_settings) {
            startActivity(new Intent(this, MySettingsActivity.class));
        } else if (id == R.id.ice_cream) {
            startActivity(new Intent(this, IceCreamSiren.class));
        } else if (id == R.id.job_requests) {
            startActivity(new Intent(this, ShopActivity.class));
        } else if (id == R.id.edit_shop) {
            Intent intent = new Intent(this, ShopActivity.class);
            intent.putExtra("just", "name");
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    protected void smartPeopleChange(List<PersonEntry> people) {
        super.smartPeopleChange(people);
        if (!JobsManager.newInstance(this).shouldLoadPeople())
            return;

        for (PersonEntry entry : people) {
            Log.d("tony", "entry.getDataPayload()" + entry.getDataPayload() + entry.getUserToken());
            Log.d("tony", "entry.getActivityString()" + entry.getActivityString());
            Log.d("tony", "entry.getDistanceFromUser" + entry.getDistanceFromUser());
        }
        JobsManager.newInstance(this).setPeopleAround(people);
    }


    @Override
    protected void smartAddressChange(AddressEntry address) {
        super.smartAddressChange(address);
        if (address != null && mLocationEt.getText().length() == 0) {
            mLocationEt.setText(address.getLocality() + "," + address.getSubAdminArea());
            Cookie.sUserLat = address.getLat();
            Cookie.sUserLong = address.getLng();

            Cookie.sMyAddress = address.getSubLocality() + "; " + address.getLocality() + "; " + address.getPremises() + "; "
                    + address.getSubAdminArea() + "; " + address.getAdminArea() + "; " + address.getPostalCode();

            Log.d("tony", Cookie.sMyAddress);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }
}
