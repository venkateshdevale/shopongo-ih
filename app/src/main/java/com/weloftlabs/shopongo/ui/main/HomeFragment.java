package com.weloftlabs.shopongo.ui.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.trnql.smart.people.PersonEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.global.MyApp;
import com.weloftlabs.shopongo.managers.JobsManager;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;
import com.weloftlabs.shopongo.utils.BlurBuilder;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements OnClickListener {

    private EditText mSearchEt;
    private ListView mSearchLv;
    private RelativeLayout mSearchLayout;
    private List<ParseObject> mSearchResultList;
    private SearchResultAdapter mAdapter;
    private ScrollView mScrollView;

    public HomeFragment() {
        mSearchResultList = new ArrayList<>();
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(root);


        return root;
    }

    private void initViews(View root) {
        mSearchEt = (EditText) root.findViewById(R.id.search_et);

        mSearchLayout = (RelativeLayout) root.findViewById(R.id.search_layout);

        root.findViewById(R.id.search_iv).setOnClickListener(this);
        root.findViewById(R.id.clear_tv).setOnClickListener(this);

        root.findViewById(R.id.food_cart).setOnClickListener(this);
        root.findViewById(R.id.technology).setOnClickListener(this);
        root.findViewById(R.id.hardware).setOnClickListener(this);
        root.findViewById(R.id.tutor).setOnClickListener(this);
        root.findViewById(R.id.entertainment).setOnClickListener(this);
        root.findViewById(R.id.baby_care).setOnClickListener(this);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.food_van1);
        Bitmap b = BlurBuilder.blur(bitmap, getActivity());
        root.findViewById(R.id.food_cart).setBackground(new BitmapDrawable(getResources(), b));

        Bitmap b1 = BlurBuilder.blur(BitmapFactory.decodeResource(getResources(), R.drawable.tech1), getActivity());
        root.findViewById(R.id.technology).setBackground(new BitmapDrawable(getResources(), b1));

        Bitmap b2 = BlurBuilder.blur(BitmapFactory.decodeResource(getResources(), R.drawable.mechanic_word1), getActivity());
        root.findViewById(R.id.hardware).setBackground(new BitmapDrawable(getResources(), b2));

        Bitmap b3 = BlurBuilder.blur(BitmapFactory.decodeResource(getResources(), R.drawable.tutor1), getActivity());
        root.findViewById(R.id.tutor).setBackground(new BitmapDrawable(getResources(), b3));

        Bitmap b4 = BlurBuilder.blur(BitmapFactory.decodeResource(getResources(), R.drawable.entertainment), getActivity());
        root.findViewById(R.id.entertainment).setBackground(new BitmapDrawable(getResources(), b4));

        Bitmap b5 = BlurBuilder.blur(BitmapFactory.decodeResource(getResources(), R.drawable.personal_care1), getActivity());
        root.findViewById(R.id.baby_care).setBackground(new BitmapDrawable(getResources(), b5));

        mSearchLv = (ListView) root.findViewById(R.id.search_lv);

        mAdapter = new SearchResultAdapter(getContext(), mSearchResultList);
        mSearchLv.setAdapter(mAdapter);

        mScrollView = (ScrollView) root.findViewById(R.id.scrollview);

        mSearchLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ShopDetailActivity.class);
                Cookie.sSearchResult = mSearchResultList.get(position);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_iv:
                doSearch(mSearchEt.getText().toString());
                break;
            case R.id.clear_tv:
                mSearchLayout.setVisibility(View.GONE);
                mSearchLv.setVisibility(View.GONE);
                break;
            case R.id.food_cart:
                doSearch(MyApp.sCategoriesList[1]);
                break;
            case R.id.technology:
                doSearch(MyApp.sCategoriesList[3]);
                break;
            case R.id.hardware:
                doSearch(MyApp.sCategoriesList[2]);
                break;
            case R.id.tutor:
                doSearch(MyApp.sCategoriesList[4]);
                break;
            case R.id.entertainment:
                doSearch(MyApp.sCategoriesList[0]);
                break;
            case R.id.baby_care:
                doSearch(MyApp.sCategoriesList[5]);
                break;
        }

    }

    private void doSearch(String search) {
        mSearchResultList.clear();

        final int searchRange;
        if (SharedPreferenceManager
                .newInstance(getContext()).getSavedValue(Constants.SEARCH_RANGE).length() == 0) {
            searchRange = 8000;
        } else {
            searchRange = Integer.valueOf(SharedPreferenceManager
                    .newInstance(getContext()).getSavedValue(Constants.SEARCH_RANGE));
        }
        final Dialog d = AlertUtils.showDialog(getContext(), "Searching");
        ParseQuery q = new ParseQuery("ShopDetails");
        q.whereContains(Constants.ShopDetails.SHOP_CATEGORY, search.trim().toLowerCase());
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                d.dismiss();

                if (e == null) {
                    if (list == null || list.size() == 0) {
                        Toast.makeText(getContext(), "Results not found", Toast.LENGTH_LONG).show();
                        return;
                    }
                    for (int i = 0; i < list.size(); i++) {
                        for (PersonEntry p : JobsManager.newInstance(getActivity()).getPeopleAroundList()) {
                            if (list.get(i).getString(Constants.ShopDetails.FB_USER_ID).equalsIgnoreCase(p.getUserToken())) {
                                if (p.getDistanceFromUser() < searchRange) {
                                    mSearchResultList.add(list.get(i));
                                    mAdapter.notifyData(mSearchResultList);
                                } else {
                                    mSearchResultList.add(list.get(i));
                                    mAdapter.notifyData(mSearchResultList);
                                }
                            }
                        }
                    }

                    mAdapter.notifyData(mSearchResultList);
                    mSearchLayout.setVisibility(View.VISIBLE);
                    mSearchLv.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getContext(), "Results not found", Toast.LENGTH_LONG).show();
                }

                mScrollView.fullScroll(View.FOCUS_UP);


            }

        });
    }
}
