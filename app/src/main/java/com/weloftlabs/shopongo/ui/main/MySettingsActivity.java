package com.weloftlabs.shopongo.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;

public class MySettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        if (SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SEARCH_RANGE).length() == 0) {
            ((EditText) findViewById(R.id.range))
                    .setText("8000");
        } else {
            ((EditText) findViewById(R.id.range))
                    .setText(SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SEARCH_RANGE));
        }
        findViewById(R.id.save_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MySettingsActivity.this, "Settings updated", Toast.LENGTH_LONG).show();
                SharedPreferenceManager.newInstance(MySettingsActivity.this)
                        .saveValue(Constants.SEARCH_RANGE, ((EditText) findViewById(R.id.range)).getText().toString());
                finish();
            }
        });

        findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
