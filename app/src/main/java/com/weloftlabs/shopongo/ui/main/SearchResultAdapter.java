package com.weloftlabs.shopongo.ui.main;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.JobsManager;

import java.util.List;

public class SearchResultAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<ParseObject> mSearchResultList;

    public SearchResultAdapter(Context context, List<ParseObject> searchResultList) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSearchResultList = searchResultList;
    }

    @Override
    public int getCount() {
        return mSearchResultList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.item_search_result, parent, false);

        ((TextView) convertView.findViewById(R.id.shop_name))
                .setText(Html.fromHtml("Shop Name: <b>" + mSearchResultList.get(position).getString(Constants.ShopDetails.SHOP_NAME) + "</b>"));

        ((TextView) convertView.findViewById(R.id.minimum_cost))
                .setText(Html.fromHtml("Minimum Charges: <b>"
                        + mSearchResultList.get(position).getString(Constants.ShopDetails.MINIMUM_CHARGES) + "</b>"));

        if (JobsManager.newInstance((Activity) mContext)
                .getPeopleAroundList().get(position).getDistanceFromUser() < 0) {
            ((TextView) convertView.findViewById(R.id.distance_tv))
                    .setText(Html.fromHtml("Dist \n <b>" + 0 + "</b>") + "\nmtrs");

        } else {
            ((TextView) convertView.findViewById(R.id.distance_tv))
                    .setText(Html.fromHtml("Dist \n <b>" + JobsManager.newInstance((Activity) mContext)
                            .getPeopleAroundList().get(position).getDistanceFromUser() + "</b>") + "\nmtrs");
        }

        return convertView;
    }

    public void notifyData(List<ParseObject> list) {
        mSearchResultList = list;
        notifyDataSetChanged();
    }
}
