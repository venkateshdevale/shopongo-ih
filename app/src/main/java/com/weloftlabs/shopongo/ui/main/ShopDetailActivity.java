package com.weloftlabs.shopongo.ui.main;

import android.content.Intent;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.parse.ParseObject;
import com.trnql.smart.people.PersonEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.global.MyApp;
import com.weloftlabs.shopongo.managers.JobsManager;

public class ShopDetailActivity extends AppCompatActivity {

    private double mLat;
    private double mLong;
    private String mShopName;
    private FrameLayout mShopBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);

        ParseObject obj = Cookie.sSearchResult;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        ((TextView) toolbar.findViewById(R.id.title_tv)).setText(obj.getString(Constants.ShopDetails.SHOP_NAME));
        toolbar.findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSupportActionBar(toolbar);

        mShopBanner = (FrameLayout) findViewById(R.id.shop_banner);

        mShopName = obj.getString(Constants.ShopDetails.SHOP_NAME);

        ((TextView) findViewById(R.id.shop_name))
                .setText(Html.fromHtml("Shop Name: <b>" + obj.getString(Constants.ShopDetails.SHOP_NAME) + "</b>"));
        ((TextView) findViewById(R.id.shop_owner))
                .setText(Html.fromHtml("Shop Owner: <b>" + obj.getString(Constants.ShopDetails.OWNER_NAME) + "</b>"));
        ((TextView) findViewById(R.id.shop_contact))
                .setText(Html.fromHtml("Shop Contact: <b>" + obj.getString(Constants.ShopDetails.SHOP_PHONE) + "</b>"));
        ((TextView) findViewById(R.id.start_time))
                .setText(Html.fromHtml("Opens At(hrs): <b>" + obj.getString(Constants.ShopDetails.SHOP_TIME_START) + "</b>"));
        ((TextView) findViewById(R.id.minimum_cost))
                .setText(Html.fromHtml("Minimum Cost: <b>" + obj.getString(Constants.ShopDetails.MINIMUM_CHARGES) + "</b>"));
        ((TextView) findViewById(R.id.shop_category))
                .setText(Html.fromHtml("Shop Name: <b>" + obj.getString(Constants.ShopDetails.SHOP_CATEGORY) + "</b>"));
        ((TextView) findViewById(R.id.service_provided_tv))
                .setText(Html.fromHtml("Services Provided: <b>" + obj.getString(Constants.ShopDetails.SERVICES_PROVIDED) + "</b>"));

        findViewById(R.id.book_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment bookServiceFrag = new BookServiceFragment();
                bookServiceFrag.show(getSupportFragmentManager(), "bookService");
            }
        });

        for (PersonEntry entry : JobsManager.newInstance(this).getPeopleAroundList()) {
            if (entry.getUserToken().equalsIgnoreCase(obj.getString(Constants.ShopDetails.FB_USER_ID))) {
                if (entry.getActivityString().equalsIgnoreCase("still")) {
                    ((TextView) findViewById(R.id.shop_status)).setText("Shop is free");
                } else if (entry.getActivityString().toLowerCase().contains("move")
                        || entry.getActivityString().toLowerCase().contains("vehicle")
                        || entry.getActivityString().toLowerCase().contains("running")) {
                    ((TextView) findViewById(R.id.shop_status)).setText("Shop is busy or on move");
                } else {
                    ((TextView) findViewById(R.id.shop_status)).setText("Shop seems to be free");
                }
                mLat = entry.getLatitude();
                mLong = entry.getLongitude();
                if (entry.getDistanceFromUser() < 0) {
                    ((TextView) findViewById(R.id.shop_distance)).setText(0 + " mtrs away");
                } else {
                    ((TextView) findViewById(R.id.shop_distance)).setText(entry.getDistanceFromUser() + " mtrs away");
                }
            }
        }

        findViewById(R.id.show_on_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW
                        , Uri.parse("geo:" + mLat + "," + mLong + "?q=" + mLat + "," + mLong + "(" + mShopName + ")"));
                startActivity(intent);
            }
        });

        for (int i = 0; i < MyApp.sCategoriesList.length; i++) {
            if (MyApp.sCategoriesList[i].equalsIgnoreCase(obj.getString(Constants.ShopDetails.SHOP_CATEGORY))) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mShopBanner.setBackground(getDrawable(MyApp.sCategoryImages[i]));
                } else {
                    mShopBanner.setBackground(getResources().getDrawable(MyApp.sCategoryImages[i]));
                }
            }
        }

    }
}
