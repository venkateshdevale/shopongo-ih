package com.weloftlabs.shopongo.ui.myrequests;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.trnql.smart.people.PersonEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.JobsManager;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;

import java.util.ArrayList;
import java.util.List;


public class MyRequestsAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<ParseObject> mMyOrdersList;
    private List<ParseObject> mShopDetailList;

    public MyRequestsAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMyOrdersList = new ArrayList<>();
        mShopDetailList = new ArrayList<>();

        final Dialog d = AlertUtils.showDialog(mContext, "Loading");
        ParseQuery q = new ParseQuery("ShopRequest");
        q.whereEqualTo(Constants.ShopRequest.CLIENT_FB_ID
                , SharedPreferenceManager.newInstance(mContext).getSavedValue(Constants.FB_USER_ID));
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    mMyOrdersList = new ArrayList<ParseObject>();
                    if (list.size() == 0) {
                        Toast.makeText(mContext, "No requests placed. Try placing one in Home", Toast.LENGTH_LONG).show();
                        d.dismiss();
                    }
                    for (final ParseObject obj : list) {
                        ParseQuery q = new ParseQuery("ShopDetails");
                        q.whereEqualTo(Constants.ShopDetails.FB_USER_ID,
                                obj.getString(Constants.ShopRequest.SHOP_ID));
                        q.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject parseObject, ParseException e) {
                                if (e == null) {
                                    mMyOrdersList.add(obj);
                                    mShopDetailList.add(parseObject);
                                    notifyDataSetChanged();
                                } else {
                                    Toast.makeText(mContext
                                            , "Error loading data from server. Try again"
                                            , Toast.LENGTH_LONG).show();
                                }

                                d.dismiss();
                            }

                        });
                    }

                }
            }

        });

    }

    @Override
    public int getCount() {
        return mShopDetailList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.item_my_requests, parent, false);

        TextView shopName = (TextView) convertView.findViewById(R.id.client_name);
        TextView shopContact = (TextView) convertView.findViewById(R.id.client_contact);
        TextView requestedTime = (TextView) convertView.findViewById(R.id.requested_time);
        TextView jobRequest = (TextView) convertView.findViewById(R.id.job_request);

        final ParseObject singleObj = mMyOrdersList.get(position);
        final ParseObject shopDetailObj = mShopDetailList.get(position);

        shopName.setText(Html.fromHtml("Shop Name: <b>" + shopDetailObj.getString(Constants.ShopDetails.SHOP_NAME) + "</b>"));
        shopContact.setText(Html.fromHtml("Shop Contact: <b>" + shopDetailObj.getString(Constants.ShopDetails.SHOP_PHONE) + "</b>"));
        requestedTime.setText(Html.fromHtml("Requested Time: <b>" + singleObj.getString(Constants.ShopRequest.EXPECTED_TIME) + "</b>"));
        jobRequest.setText(Html.fromHtml("Job Request : <b>" + singleObj.getString(Constants.ShopRequest.JOB_REQUEST) + "</b>"));

        convertView.findViewById(R.id.call_shop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + shopDetailObj.getString(Constants.ShopDetails.SHOP_PHONE)));
                mContext.startActivity(intent);
            }
        });

        convertView.findViewById(R.id.track_shop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (PersonEntry entry : JobsManager.newInstance((Activity) mContext).getPeopleAroundList()) {
                    if (entry.getUserToken().equalsIgnoreCase(shopDetailObj.getString(Constants.ShopDetails.FB_USER_ID))) {
                        Intent intent = new Intent(Intent.ACTION_VIEW
                                , Uri.parse("geo:" + entry.getLatitude() + "," + entry.getLongitude()
                                + "?q=" + entry.getLatitude() + "," + entry.getLongitude()
                                + "(" + shopDetailObj.getString(Constants.ShopDetails.SHOP_NAME) + ")"));
                        mContext.startActivity(intent);
                        break;
                    }
                }


            }
        });


        return convertView;
    }


}
