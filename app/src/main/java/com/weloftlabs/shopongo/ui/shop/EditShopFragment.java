package com.weloftlabs.shopongo.ui.shop;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.MyApp;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;

import java.util.Calendar;

public class EditShopFragment extends Fragment implements AdapterView.OnItemSelectedListener, TimePickerDialog.OnTimeSetListener {

    private Spinner mCategorySpinner;
    private int mPlacePosition = 0;
    private TextView mTimeTv;
    private ParseObject mShopDetailsObj;

    public EditShopFragment() {

    }

    public static EditShopFragment newInstance() {
        EditShopFragment fragment = new EditShopFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_shop_setup, container, false);

        initViews(root);

        return root;
    }

    private void initViews(View root) {
        final EditText shopName = (EditText) root.findViewById(R.id.shop_name_et);
        final EditText shopPhone = (EditText) root.findViewById(R.id.shop_phone_et);
        final EditText serviceProvided = (EditText) root.findViewById(R.id.service_provided_et);
        final EditText minimumCost = (EditText) root.findViewById(R.id.minimum_cost);

        mTimeTv = (TextView) root.findViewById(R.id.timing_tv);
        mTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        EditShopFragment.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        mCategorySpinner = (Spinner) root.findViewById(R.id.shop_category_spinner);

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_tv, MyApp.sCategoriesList);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorySpinner.setAdapter(spinnerAdapter);
        mCategorySpinner.setOnItemSelectedListener(this);

        root.findViewById(R.id.start_shop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShopDetailsObj.put(Constants.ShopDetails.SHOP_NAME, shopName.getText().toString());
                mShopDetailsObj.put(Constants.ShopDetails.SHOP_PHONE, shopPhone.getText().toString());
                mShopDetailsObj.put(Constants.ShopDetails.SERVICES_PROVIDED, serviceProvided.getText().toString());
                mShopDetailsObj.put(Constants.ShopDetails.SHOP_TIME_START, mTimeTv.getText().toString());
                mShopDetailsObj.put(Constants.ShopDetails.SHOP_CATEGORY, MyApp.sCategoriesList[mPlacePosition]);
                mShopDetailsObj.put(Constants.ShopDetails.MINIMUM_CHARGES, minimumCost.getText().toString());

                final Dialog d = AlertUtils.showDialog(getActivity(), "Updating stores");
                mShopDetailsObj.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            d.dismiss();
                            Toast.makeText(getContext(), "Store data updated", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        final Dialog d = AlertUtils.showDialog(getContext(), "Loading data");
        ParseQuery q = new ParseQuery("ShopDetails");
        q.whereEqualTo(Constants.ShopDetails.FB_USER_ID
                , SharedPreferenceManager.newInstance(getContext()).getSavedValue(Constants.FB_USER_ID));
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    mShopDetailsObj = object;
                    shopName.setText(object.getString(Constants.ShopDetails.SHOP_NAME));
                    shopPhone.setText(object.getString(Constants.ShopDetails.SHOP_PHONE));
                    minimumCost.setText(object.getString(Constants.ShopDetails.MINIMUM_CHARGES));
                    serviceProvided.setText(object.getString(Constants.ShopDetails.SERVICES_PROVIDED));
                    mTimeTv.setText(object.getString(Constants.ShopDetails.SHOP_TIME_START));
                    for (int i = 0; i < MyApp.sCategoriesList.length; i++) {
                        if (object.getString(Constants.ShopDetails.SHOP_CATEGORY).equalsIgnoreCase(MyApp.sCategoriesList[i])) {
                            mCategorySpinner.setSelection(i);
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "Error loading data, try again", Toast.LENGTH_LONG).show();
                }
                d.dismiss();
            }

        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mPlacePosition = position;
        switch (view.getId()) {
            case R.id.shop_category_spinner:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mTimeTv.setText("" + hourOfDay + ":" + minute);
    }
}
