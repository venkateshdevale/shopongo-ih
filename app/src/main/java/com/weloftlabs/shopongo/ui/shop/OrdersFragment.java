package com.weloftlabs.shopongo.ui.shop;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.trnql.smart.base.SmartCompatFragment;
import com.trnql.smart.weather.WeatherEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.custom.tv.LatoHeavyTv;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.MyApp;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;

public class OrdersFragment extends SmartCompatFragment {

    private ListView mOrdersLv;

    private LatoHeavyTv mCurrentTmp;
    private LatoHeavyTv mFeelsLike;
    private LatoHeavyTv mCurrentCondition;
    private ImageView mWeatherIv;
    private FrameLayout mShopBanner;

    public OrdersFragment() {

    }

    public static OrdersFragment newInstance() {
        OrdersFragment fragment = new OrdersFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_orders, container, false);

        initViews(root);

        return root;
    }

    private void initViews(final View root) {
        mShopBanner = (FrameLayout) root.findViewById(R.id.shop_banner);
        mCurrentTmp = (LatoHeavyTv) root.findViewById(R.id.current_temp);
        mFeelsLike = (LatoHeavyTv) root.findViewById(R.id.feels_like);
        mCurrentCondition = (LatoHeavyTv) root.findViewById(R.id.current_condition);
        mWeatherIv = (ImageView) root.findViewById(R.id.weather_iv);

        root.findViewById(R.id.shop_name).bringToFront();

        ParseQuery q = new ParseQuery("ShopDetails");
        q.whereEqualTo(Constants.FB_USER_ID, SharedPreferenceManager.newInstance(getContext())
                .getSavedValue(Constants.FB_USER_ID));
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                ((TextView) root.findViewById(R.id.shop_name))
                        .setText(parseObject.getString(Constants.ShopDetails.SHOP_NAME));

                for (int i = 0; i < MyApp.sCategoriesList.length; i++) {
                    if (MyApp.sCategoriesList[i].equalsIgnoreCase(parseObject.getString(Constants.ShopDetails.SHOP_CATEGORY))) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            mShopBanner.setBackground(getActivity().getDrawable(MyApp.sCategoryImages[i]));
                        } else {
                            mShopBanner.setBackground(getActivity().getResources().getDrawable(MyApp.sCategoryImages[i]));
                        }
                    }
                }
            }

        });

        mOrdersLv = (ListView) root.findViewById(R.id.orders_lv);

        ShopOrdersAdapter adapter = new ShopOrdersAdapter(getContext());
        mOrdersLv.setAdapter(adapter);

    }

    @Override
    protected void smartWeatherChange(WeatherEntry weather) {
        super.smartWeatherChange(weather);

        Log.d("tony", "weather");
        if (weather != null) {
            mCurrentTmp.setText(Html.fromHtml("Current Temperature: <b>" + weather.getCurrentTemp() + "</b>"));
            mFeelsLike.setText(Html.fromHtml("Feels Like: <b>" + weather.getFeelsLikeAsString() + "</b>"));
            mCurrentCondition.setText(Html.fromHtml("Current Condition: <b>"
                    + weather.getCurrentConditionsDescriptionAsString() + "</b>"));

            if (weather.getCurrentConditionsDescriptionAsString().toLowerCase().contains("cloud")) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.cloudy, null));
                } else {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.cloudy));
                }
            } else if (weather.getCurrentConditionsDescriptionAsString().toLowerCase().contains("rain")) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.rainy, null));
                } else {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.rainy));
                }
            } else if (weather.getCurrentConditionsDescriptionAsString().toLowerCase().contains("sun")) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.sunny, null));
                } else {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.sunny));
                }
            } else if (weather.getCurrentConditionsDescriptionAsString().toLowerCase().contains("snow")) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.snowy, null));
                } else {
                    mWeatherIv.setBackground(getResources().getDrawable(R.drawable.snowy));
                }
            }
        }
    }
}
