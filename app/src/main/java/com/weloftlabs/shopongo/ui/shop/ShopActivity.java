package com.weloftlabs.shopongo.ui.shop;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;
import com.weloftlabs.shopongo.utils.BlurBuilder;
import com.weloftlabs.shopongo.utils.FragmentHelper;

public class ShopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        initViews();
    }

    private void initViews() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.title_tv)).setText("My Shop");
        toolbar.setTitle("");
        toolbar.findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
        Bitmap blurredBitmap = BlurBuilder.blur(bitmap, ShopActivity.this);
        findViewById(R.id.parent).setBackground(new BitmapDrawable(getResources(), blurredBitmap));

        if (getIntent().getExtras() != null) {
            FragmentHelper.replaceAndAddToStack(this, R.id.container, EditShopFragment.newInstance());
        } else {
            final Dialog d = AlertUtils.showDialog(this, "Loading data");
            ParseQuery q = new ParseQuery("ShopDetails");
            q.whereEqualTo(Constants.ShopDetails.FB_USER_ID,
                    SharedPreferenceManager.newInstance(this).getSavedValue(Constants.FB_USER_ID));
            q.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (parseObject != null) {
                        FragmentHelper.replaceAndAddToStack(ShopActivity.this, R.id.container, OrdersFragment.newInstance());
                    } else {
                        if (!SharedPreferenceManager.newInstance(ShopActivity.this).getSavedStates(Constants.SHOP_SETUP)) {
                            ((TextView) toolbar.findViewById(R.id.title_tv)).setText("My Shop Setup");
                            FragmentHelper.replaceAndAddToStack(ShopActivity.this, R.id.container, ShopSetupFragment.newInstance());
                        } else {
                            ((TextView) toolbar.findViewById(R.id.title_tv)).setText("My Shop Orders");
                            FragmentHelper.replaceAndAddToStack(ShopActivity.this, R.id.container, OrdersFragment.newInstance());
                        }
                    }
                    d.dismiss();
                }
            });

        }
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
