package com.weloftlabs.shopongo.ui.shop;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.trnql.smart.people.PersonEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.managers.JobsManager;
import com.weloftlabs.shopongo.managers.MyShopManager;
import com.weloftlabs.shopongo.utils.AlertUtils;

import java.util.ArrayList;
import java.util.List;


public class ShopOrdersAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<ParseObject> mOrdersList;
    private List<ParseObject> mClientDetailList;

    public ShopOrdersAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mOrdersList = new ArrayList<>();

        final Dialog d = AlertUtils.showDialog(mContext, "Loading");
        MyShopManager.newInstance((Activity) mContext).getMyShopOrders(new MyShopManager.OrdersCallback() {
            @Override
            public void success(List<ParseObject> ordersList, List<ParseObject> clientDetailList) {
                d.dismiss();
                mOrdersList = ordersList;
                mClientDetailList = clientDetailList;
                notifyDataSetChanged();
            }

            @Override
            public void fail() {
                d.dismiss();
                Toast.makeText(mContext, "Error during loading orders, try to restart the app", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getCount() {
        return mOrdersList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.item_shop_orders, parent, false);

        TextView clientName = (TextView) convertView.findViewById(R.id.client_name);
        TextView clientContact = (TextView) convertView.findViewById(R.id.client_contact);
        TextView clientAddress = (TextView) convertView.findViewById(R.id.client_address);
        TextView requestedTime = (TextView) convertView.findViewById(R.id.requested_time);
        TextView jobRequest = (TextView) convertView.findViewById(R.id.job_request);
        final CheckBox isDone = (CheckBox) convertView.findViewById(R.id.isDone);

        final ParseObject singleObj = mOrdersList.get(position);
        final ParseObject clientObj = mClientDetailList.get(position);

        isDone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                ParseQuery q = new ParseQuery("ShopRequest");
                q.getInBackground(singleObj.getObjectId(), new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        if (e == null) {
                            parseObject.put(Constants.ShopRequest.IS_DONE, isChecked);
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e != null) {
                                        isDone.setChecked(!isChecked);
                                    } else {
                                        Toast.makeText(mContext, "Marked as done", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                        }
                    }

                });
            }
        });

        clientName.setText(Html.fromHtml("Client Name: <b>" + clientObj.getString(Constants.FbUser.NAME) + "</b>"));
        clientContact.setText(Html.fromHtml("Client Contact: <b>" + clientObj.getString(Constants.FbUser.USER_CONTACT) + "</b>"));
        clientAddress.setText(Html.fromHtml("Client Address: <b>" + singleObj.getString(Constants.ShopRequest.CLIENT_ADDRESS) + "</b>"));
        requestedTime.setText(Html.fromHtml("Requested Time: <b>" + singleObj.getString(Constants.ShopRequest.EXPECTED_TIME) + "</b>"));
        isDone.setChecked(singleObj.getBoolean(Constants.ShopRequest.IS_DONE));
        jobRequest.setText(Html.fromHtml("Job Description: <b>" + singleObj.getString(Constants.ShopRequest.JOB_REQUEST) + "</b>"));

        convertView.findViewById(R.id.call_client).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + clientObj.getString(Constants.FbUser.USER_CONTACT)));
                mContext.startActivity(intent);
            }
        });

        convertView.findViewById(R.id.navigate_to_client).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (PersonEntry entry : JobsManager.newInstance((Activity) mContext).getPeopleAroundList()) {
                    if (clientObj.getString(Constants.FB_USER_ID).equalsIgnoreCase(entry.getUserToken())) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=" + Cookie.sUserLat + "," + Cookie.sUserLong
                                        + "&daddr=" + entry.getLatitude()
                                        + "," + entry.getLongitude()));
                        mContext.startActivity(intent);
                        break;
                    }
                }

            }
        });

        return convertView;
    }


}
