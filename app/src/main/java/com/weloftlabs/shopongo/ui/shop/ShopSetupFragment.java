package com.weloftlabs.shopongo.ui.shop;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.MyApp;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.utils.AlertUtils;
import com.weloftlabs.shopongo.utils.FragmentHelper;

import java.util.Calendar;

public class ShopSetupFragment extends Fragment implements AdapterView.OnItemSelectedListener, TimePickerDialog.OnTimeSetListener {

    private Spinner mCategorySpinner;
    private int mPlacePosition = 0;
    private TextView mTimeTv;

    public ShopSetupFragment() {

    }

    public static ShopSetupFragment newInstance() {
        ShopSetupFragment fragment = new ShopSetupFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_shop_setup, container, false);

        initViews(root);

        return root;
    }

    private void initViews(View root) {
        final EditText shopName = (EditText) root.findViewById(R.id.shop_name_et);
        final EditText shopPhone = (EditText) root.findViewById(R.id.shop_phone_et);
        final EditText serviceProvided = (EditText) root.findViewById(R.id.service_provided_et);
        final EditText minimumCost = (EditText) root.findViewById(R.id.minimum_cost);

        mTimeTv = (TextView) root.findViewById(R.id.timing_tv);
        mTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        ShopSetupFragment.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        mCategorySpinner = (Spinner) root.findViewById(R.id.shop_category_spinner);

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.spinner_tv, MyApp.sCategoriesList);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorySpinner.setAdapter(spinnerAdapter);
        mCategorySpinner.setOnItemSelectedListener(this);

        root.findViewById(R.id.start_shop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseObject object = new ParseObject("ShopDetails");
                object.put("fbUserId", SharedPreferenceManager.newInstance(getContext()).getSavedValue(Constants.FB_USER_ID));
                object.put(Constants.ShopDetails.SHOP_NAME, shopName.getText().toString());
                object.put(Constants.ShopDetails.SHOP_PHONE, shopPhone.getText().toString());
                object.put(Constants.ShopDetails.MINIMUM_CHARGES, minimumCost.getText().toString());
                object.put(Constants.ShopDetails.SERVICES_PROVIDED, serviceProvided.getText().toString());
                object.put(Constants.ShopDetails.SHOP_TIME_START, mTimeTv.getText().toString());
                object.put(Constants.ShopDetails.SHOP_CATEGORY, MyApp.sCategoriesList[mPlacePosition].toLowerCase());
                object.put(Constants.ShopDetails.OWNER_NAME,
                        SharedPreferenceManager.newInstance(getContext()).getSavedValue(Constants.USER_NAME));

                final Dialog d = AlertUtils.showDialog(getActivity(), "Starting stores");
                object.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            d.dismiss();
                            SharedPreferenceManager.newInstance(getActivity()).saveStateValues(Constants.SHOP_SETUP, true);
                            FragmentHelper.replace(getActivity(), R.id.container, OrdersFragment.newInstance());
                        } else {
                            Toast.makeText(getContext(),
                                    "Error starting a store. Try again. If problem persist send a mail at support@weloftlabs.com", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mPlacePosition = position;
        if (view != null) {
            switch (view.getId()) {
                case R.id.shop_category_spinner:
                    mPlacePosition = position;
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mTimeTv.setText("" + hourOfDay + ":" + minute);
    }
}
