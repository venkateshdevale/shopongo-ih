package com.weloftlabs.shopongo.ui.signin;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.ui.main.HomeActivity;
import com.weloftlabs.shopongo.utils.AlertUtils;
import com.weloftlabs.shopongo.utils.BlurBuilder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    private LoginButton mLoginButton;
    private CallbackManager callbackManager;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        if (AccessToken.getCurrentAccessToken() != null) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }

        printKeyHash();

        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mLoginButton.setReadPermissions("user_friends");

        LoginManager.getInstance();

        callbackManager = CallbackManager.Factory.create();

        mLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                ParseQuery<ParseObject> query = ParseQuery.getQuery("FbUser");
                query.whereEqualTo("fbUserId", loginResult.getAccessToken().getUserId());
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject userObj, com.parse.ParseException e) {
                        if (e == null) {

                            SharedPreferenceManager.newInstance(LoginActivity.this).saveValue(Constants.FB_USER_ID, loginResult.getAccessToken().getUserId());

                            // Sign In
                            Intent intent = new Intent(LoginActivity.this, UserDetailsActivity.class);
                            intent.putExtra("isSignup", false);
                            intent.putExtra("fbUserId", loginResult.getAccessToken().getUserId());
                            intent.putExtra("name", userObj.getString("name"));
                            intent.putExtra("email", userObj.getString("email"));
                            intent.putExtra("userContact", userObj.getString("userContact"));
                            intent.putExtra("isSubscribed", userObj.getBoolean("isSubscribed"));

                            startActivity(intent);

                            Toast.makeText(LoginActivity.this, "Signed in successfully", Toast.LENGTH_LONG).show();

                            finish();

                        } else {
                            if (e.getCode() == 101) {
                                // Sign Up
                                final Dialog dialog = AlertUtils.showDialog(LoginActivity.this, "Signing Up. Please wait");

                                ParseObject obj = new ParseObject("FbUser");
                                obj.put("fbUserId", loginResult.getAccessToken().getUserId());
                                obj.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(com.parse.ParseException e) {
                                        if (e == null) {

                                            SharedPreferenceManager.newInstance(LoginActivity.this).saveValue(Constants.FB_USER_ID, loginResult.getAccessToken().getUserId());

                                            Intent intent = new Intent(LoginActivity.this, UserDetailsActivity.class);
                                            intent.putExtra("isSignup", true);
                                            intent.putExtra("fbUserId", loginResult.getAccessToken().getUserId());
                                            startActivity(intent);

                                            finish();
                                        } else {
                                            Toast.makeText(LoginActivity.this, "Error happened while signing up. Please try again", Toast.LENGTH_LONG).show();
                                        }
                                        dialog.dismiss();
                                    }

                                });

                            } else {
                                Toast.makeText(LoginActivity.this, "Error happened while signing in. Please try again", Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                });

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sign_in_bg);
                Bitmap blurredBitmap = BlurBuilder.blur(bitmap, LoginActivity.this);

                findViewById(R.id.login_bg).setBackground(new BitmapDrawable(getResources(), blurredBitmap));
                findViewById(R.id.logo).setVisibility(View.VISIBLE);
                findViewById(R.id.logo_tv).setVisibility(View.VISIBLE);

                mLoginButton.setVisibility(View.VISIBLE);
            }
        }, 1500);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.weloftlabs.wownote",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }

}
