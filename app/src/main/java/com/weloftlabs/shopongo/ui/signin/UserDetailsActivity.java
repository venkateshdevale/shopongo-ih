package com.weloftlabs.shopongo.ui.signin;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.Profile;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.ui.main.HomeActivity;
import com.weloftlabs.shopongo.utils.AlertUtils;

public class UserDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean mIsSignUp;
    private boolean mIsSubscribed;
    private String mFbUserId;
    private EditText mUserName;
    private EditText mUserEmail;
    private EditText mUserContact;
    private Button mProceed;
    private SwitchCompat mSubscribe;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        initView();

        Bundle bundle;
        if ((bundle = getIntent().getExtras()) != null) {
            mIsSignUp = bundle.getBoolean("isSignup");
            mFbUserId = bundle.getString("fbUserId");

            mSubscribe.setChecked(true);

            if (!mIsSignUp) {
                mUserName.setText(bundle.getString("name"));
                mUserEmail.setText(bundle.getString("email"));
                mUserContact.setText(bundle.getString("userContact") + "");
                mSubscribe.setChecked(bundle.getBoolean("isSubscribed"));
            } else {
                mUserName.setText(Profile.getCurrentProfile().getName().trim());

            }
        }

    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Details");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);

        mUserName = (EditText) findViewById(R.id.user_name);
        mUserEmail = (EditText) findViewById(R.id.email_contact);
        mUserContact = (EditText) findViewById(R.id.user_contact);
        mSubscribe = (SwitchCompat) findViewById(R.id.subscribe_switch);

        findViewById(R.id.proceed).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.proceed) {
            boolean shouldProceed = true;

            if (mUserName.getText().toString().trim().length() == 0) {
                mUserName.setError("This feild can't be empty");
                shouldProceed = false;
            }
            if (mUserContact.getText().toString().trim().length() == 0) {
                mUserContact.setError("This feild can't be empty");
                shouldProceed = false;
            }
            if (mUserEmail.getText().toString().trim().length() == 0) {
                mUserEmail.setError("This feild can't be empty");
                shouldProceed = false;
            }

            if (!shouldProceed) {
                return;

            } else {

                SharedPreferenceManager.newInstance(this).saveValue(Constants.USER_NAME, mUserName.getText().toString());
                final Dialog dialog = AlertUtils.showDialog(UserDetailsActivity.this, "Loading...");

                ParseQuery<ParseObject> query = ParseQuery.getQuery("FbUser");
                query.whereEqualTo("fbUserId", mFbUserId);
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            SharedPreferenceManager.newInstance(UserDetailsActivity.this)
                                    .saveValue(Constants.USER_NAME, mUserName.getText().toString().trim());
                            object.put("name", mUserName.getText().toString().trim());
                            object.put("email", mUserEmail.getText().toString().trim());
                            object.put("userContact", mUserContact.getText().toString().trim());
                            object.put("isSubscribed", mSubscribe.isChecked());

                            object.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    dialog.dismiss();

                                    if (e == null) {
                                        startActivity(new Intent(UserDetailsActivity.this, HomeActivity.class));
                                        finish();

                                    } else {
                                        Toast.makeText(UserDetailsActivity.this, "Couldn't save preferences, please try again", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(UserDetailsActivity.this, HomeActivity.class));
                                        finish();
                                    }
                                }
                            });
                        } else {
                            dialog.dismiss();
                            Toast.makeText(UserDetailsActivity.this, "Failed to save user data. Try again.", Toast.LENGTH_LONG).show();

                        }
                    }

                });
            }

        }
    }
}
