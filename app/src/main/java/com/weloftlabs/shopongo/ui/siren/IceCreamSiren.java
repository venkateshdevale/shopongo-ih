package com.weloftlabs.shopongo.ui.siren;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SendCallback;
import com.trnql.smart.activity.ActivityEntry;
import com.trnql.smart.base.SmartCompatActivity;
import com.trnql.smart.location.AddressEntry;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;


public class IceCreamSiren extends SmartCompatActivity {

    private double mLat;
    private double mLong;
    private boolean isFirstTime = true;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siren);

        mHandler = new Handler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);

        findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void smartAddressChange(AddressEntry address) {
        super.smartAddressChange(address);

        Log.d("stark", "address change");
        if (address != null) {
            mLat = Double.valueOf(address.getLat());
            mLong = Double.valueOf(address.getLng());

            if (isFirstTime) {
                sendPush();
                isFirstTime = false;
            }
        }
    }

    @Override
    protected void smartActivityChange(ActivityEntry userActivity) {
        super.smartActivityChange(userActivity);

        if (userActivity.isStill()) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendPush();

                }
            }, 120000);

        }
        Log.d("stark", "activity change" + userActivity.getActivityString());
    }

    private void sendPush() {

        ParseQuery q = new ParseQuery("ShopDetails");
        q.whereEqualTo(Constants.ShopDetails.FB_USER_ID,
                SharedPreferenceManager.newInstance(this).getSavedValue(Constants.FB_USER_ID));
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    JSONObject data = new JSONObject();
                    ParsePush push = new ParsePush();

                    try {
                        data.put(Constants.FB_USER_ID,
                                SharedPreferenceManager.newInstance(IceCreamSiren.this).getSavedValue(Constants.FB_USER_ID));
                        data.put(Constants.LAT, mLat);
                        data.put(Constants.LONG, mLong);
                        data.put(Constants.ShopDetails.SHOP_NAME, parseObject.getString(Constants.ShopDetails.SHOP_NAME));
                        data.put(Constants.ShopDetails.SHOP_CATEGORY, parseObject.getString(Constants.ShopDetails.SHOP_CATEGORY));

                        push.setData(data);
                        push.sendInBackground(new SendCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e != null) {
                                    Toast.makeText(IceCreamSiren.this, "Failed to send Siren notification", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(IceCreamSiren.this, "Siren sent to people nearby", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                }
            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }
}
