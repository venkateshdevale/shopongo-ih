package com.weloftlabs.shopongo.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.weloftlabs.shopongo.R;
import com.weloftlabs.shopongo.global.Constants;
import com.weloftlabs.shopongo.global.Cookie;
import com.weloftlabs.shopongo.managers.SharedPreferenceManager;
import com.weloftlabs.shopongo.ui.main.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;


public class CustomPushReceiver extends ParsePushBroadcastReceiver {
    private final String TAG = CustomPushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private Intent parseIntent;

    public CustomPushReceiver() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
        Log.e("wel", "Push received: ");

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.e(TAG, "Push received: " + json);
            parseIntent = intent;
            parsePushJson(context, json);
        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    private void parsePushJson(final Context context, final JSONObject json) {
        try {
            final String userId = json.getString(Constants.FB_USER_ID);

            showNotificationForJson(context,
                    json.getString(Constants.ShopDetails.SHOP_NAME), json.getString(Constants.ShopDetails.SHOP_CATEGORY));

            if (userId.equalsIgnoreCase(SharedPreferenceManager.newInstance(context)
                    .getSavedValue(Constants.FB_USER_ID)))
                return;


            Location myLoc = new Location("A");
            myLoc.setLatitude(Double.valueOf(Cookie.sUserLat));
            myLoc.setLongitude(Double.valueOf(Cookie.sUserLong));

            Location shopLoc = new Location("B");
            shopLoc.setLatitude(Double.valueOf(json.getString(Constants.LAT)));
            shopLoc.setLongitude(Double.valueOf(json.getString(Constants.LONG)));

            float dist = myLoc.distanceTo(shopLoc);

            Log.d("Push", "dist" + dist);

            if (dist < 200) {
                showNotificationForJson(context,
                        json.getString(Constants.ShopDetails.SHOP_NAME), json.getString(Constants.ShopDetails.SHOP_CATEGORY));
            }


        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    private void showNotificationForJson(Context context, String shopName, String category) {

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(context, HomeActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification;

        if (Build.VERSION.SDK_INT < 16) {
            notification = new Notification.Builder(context).setContentTitle("Voila " + shopName + " is around you. Check out.")
                    .setContentText("Check out for the shop in " + category)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .getNotification();
        } else {
            notification = new Notification.Builder(context)
                    .setContentTitle("Voila " + shopName + " is around you. Check out.")
                    .setContentText("Check out for the shop in " + category)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(soundUri)
                    .build();
        }

        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);


    }
}